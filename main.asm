section .data

%define BUFFER_SIZE 256
%define STDOUT 1
%define STDERR 2
%define OFFSET 8

extern find_word

%include "lib.inc"
%include "words.inc"

greeting: db 'Enter the key: ', 0
found: db 'Found a node in the dictionary: ', 0
not_found: db 'Nothing was found by entered key', 0xA, 0
overflow: db 'The key is too long', 0xA, 0

section .text
global _start

_start:
    mov rdi, greeting
    mov rsi, STDOUT
    call print_string
    sub rsp, BUFFER_SIZE
    mov rsi, BUFFER_SIZE
    mov rdi, rsp
    call read_word
    push rdx
    je .key_too_long
    mov rdi, rax
    mov rsi, current
    call find_word
    test rax, rax
    je .not_found_error

.found:
    add rax, OFFSET
    push rax
    mov rsi, rax
    call string_length
    pop rsi
    add rax, rsi
    inc rax
    push rax
    mov rdi, found
    mov rsi, STDOUT
    call print_string
    pop rdi
    mov rsi, STDOUT
    call print_string
    call print_newline
    mov rdi, 0
    jmp .end

.not_found_error:
    mov rdi, not_found
    mov rsi, STDERR
    call print_string
    mov rdi, -1
    jmp .end

.key_too_long:
    mov rdi, overflow
    mov rsi, STDERR
    call print_string
    mov rdi, -1
    jmp .end
    
.end:
    call exit
