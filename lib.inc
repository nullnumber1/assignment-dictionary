section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall

out:
    mov rax, 1
    mov rdi, 1
    syscall
    ret

in:
    mov rax, 0
    mov rdi, 0
    syscall
    ret
    

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi + rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    jmp out
; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdx, 1
    call out
    pop rax
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xa
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rdi, rsp
    dec rdi
    push 0
    sub rsp, 16
    mov r10, 10

    .loop:
        xor rdx, rdx
        div r10
        add rdx, '0'
        dec rdi
        mov byte[rdi], dl
        test rax, rax
        je .end
        jmp .loop

    .end:
        call print_string
        add rsp, 24
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int: 
	cmp rdi, 0
    jnl .print
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi

    .print:
        jmp print_uint
 
; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx

    .loop:
        mov r10b, byte[rdi + rcx]
        cmp r10b, byte[rsi + rcx]
        jne .not_equal
        cmp r10b, 0
        je .equal
        inc rcx
        jmp .loop

    .not_equal:
        xor rax, rax
        ret

    .equal:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rdx, 1
    push 0
    mov rsi, rsp
    call in
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor rcx, rcx
    mov r8, rdi
    mov r9, rsi
    mov r10, rcx

    .loop:
        call read_char
        cmp al, 0x20
        je .whitespace
        cmp al, 0x9
        je .whitespace
        cmp al, 0xa
        je .whitespace
        test rax, rax
        je .end
        mov [r8 + r10], rax
        inc r10
        cmp r10, r9
        jae .error
        jmp .loop

    .whitespace:
        test r10, r10
        je .loop
        je .end

    .end:
        mov byte [r8 + r10], 0
        mov rax, r8
        mov rdx, r10
        ret

    .error:
        xor rax, rax
        xor rdx, rdx
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rcx, rcx
    xor rax, rax
    xor rdx, rdx
    xor r8, r8

    .loop:
        mov r8b, byte[rdi + rcx]
        sub r8b, 48
        cmp r8b, 0
        jb .end
        cmp r8b, 9
        ja .end
        inc rcx
        imul rax, 10
        add rax, r8
        jmp .loop

    .end:
        mov rdx, rcx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte[rdi], '-'
    je .sign
    call parse_uint
    jmp .end

    .sign:    
        inc rdi
        call parse_uint
        cmp rdx, 0
        je .end
        neg rax
        inc rdx

    .end:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
call string_length
cmp rax, rdx
jae .too_long
inc rax
xor rcx, rcx
xor rax, rax

.loop:
    mov rcx, [rdi + rax]
    mov [rsi + rax], rcx
    inc rax
    cmp byte [rdi + rax], 0
    jne .loop
    ret

.too_long:
    xor rax, rax
    ret
