LD=ld
ASM=nasm 
ASMFLAGS=-f elf64

.PHONY: build
build: main

main.o: main.asm lib.inc words.inc colon.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

main: main.o lib.o dict.o
	$(LD) -o $@ $^

.PHONY: clean
clean:
	rm -rf *.o main