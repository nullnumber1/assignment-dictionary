%include "lib.inc"

section .text
global find_word

find_word:
.loop:
        push rsi
        add rsi, 8
        call string_equals
        pop rsi
        test rax, rax
        jne .found
        mov rsi, [rsi]
        test rsi, rsi
        je .not_found
        jmp .loop
.found:
        mov rax, rsi
        ret
.not_found:
        xor rax, rax
        ret