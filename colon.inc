%define current 0

%macro colon 2
    %%label: dq current
    db %1, 0
    %2: 
        %define current %%label
%endmacro